const style = theme => ({
  line: {
    height: 3,
    backgroundColor: theme.palette.action.selected,
    margin: '50px -30px 20px',
  },
  container: {
    padding: 30,
    overflow: 'hidden'
  },
  detail: {
    marginTop: 50,
  },
  gutterBottom: {
    marginBottom: 30,
  },
})

export default style