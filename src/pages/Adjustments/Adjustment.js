import React from 'react'
import PropTypes from 'prop-types'
import { withStyles, Grid, Typography } from '@material-ui/core'
import style from './style'
import AdjustmentForm from '../../components/main/Form/index'
import AdjustmentDetail from '../../components/main/Details/index'
import ButtonStyled from '../../components/common/Button/index'

const Adjustment = ({ classes }) => {
  return (
    <Grid container className={classes.container}>
      <Grid item xs={12}>
        <Typography align='right' variant='h6' color='primary' gutterBottom classes={{gutterBottom: `${classes.gutterBottom}`}}>Amy Nguyen</Typography>
      </Grid>
      <Grid item container xs={12}>
        <Grid item xs={12}>
          <AdjustmentForm/>
        </Grid>
        <Grid item xs={12} className={classes.detail}>
          <AdjustmentDetail/>
        </Grid>
      </Grid>
      <Grid item xs={12}>
        <div className={classes.line}></div>
        <ButtonStyled label='Save' rounded/>
      </Grid>
    </Grid>
  )
}

Adjustment.defaultProps = {
  classes: '',
}

Adjustment.propTypes = {
  classes: PropTypes.shape().isRequired,
}

export default withStyles(style)(Adjustment)
