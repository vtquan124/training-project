import React from 'react'
import Adjustment from './pages/Adjustments/Adjustment'

const App = () => {
  return (
    <div className="App">
      <Adjustment/>
    </div>
  );
}

export default App;
