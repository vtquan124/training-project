import { adjustReason}  from './adjustReason'
import { adjustType } from './adjustType'
import { product } from './product'
import { quantity } from './quantity'

import * as Yup from 'yup'

export const schemas = {
  adjustReason, adjustType, product, quantity
}

// export default schemas