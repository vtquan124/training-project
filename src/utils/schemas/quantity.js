import * as Yup from 'yup'

export const quantity = Yup.number().positive('Must be greater than 0').required('Must not be empty')