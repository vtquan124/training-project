import { Field } from 'formik'

const FieldWrapper = (Component) => {
  // <Field component/> can't use meta prop
  // If using <Field children/>, the props passed from parent components will not be passed to Component, because Component now is Field's child, not Field rendered like <Field component/>
  return (props) => (
    <Field component={Component} {...props}/>
  )
}

export default FieldWrapper