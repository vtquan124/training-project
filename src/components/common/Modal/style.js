const style = theme => ({
  modal: {
    width: 250,
    borderRadius: 10,
    padding: 20,
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%,-50%)',
    outline: 'none'
  },
  buttons: {
    display: 'flex',
    justifyContent: 'flex-end',
    '& button': {
      marginLeft: 5
    }
  }
})

export default style