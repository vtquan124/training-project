import React from 'react'
import PropTypes from 'prop-types'
import { withStyles, Modal, Typography, Paper, Button } from '@material-ui/core'
import style from './style'
import ButtonStyled from '../Button/index'

const ModalStyled = ({ classes, open, handleClose, ...props }) => {
  return (
    <Modal
      open={open}
      onClose={handleClose}
      {...props}
    >
      <Paper className={classes.modal}>
        <Typography
          variant='h6'
          color='textPrimary'
          component='h6'
          gutterBottom
        >Do you want to delete this item?</Typography>
        <div className={classes.buttons}>
          <ButtonStyled variant='text' label='Delete' color='secondary' rounded/>
          <ButtonStyled variant='text' label='Cancel' color='default' rounded handleClick={handleClose}/>
        </div>
      </Paper>
    </Modal>
  )
}

ModalStyled.defaultProps = {
  open: false,
  handleClose: ()=>{}
}

ModalStyled.propTypes = {
  classes: PropTypes.shape().isRequired,
  open: PropTypes.bool,
  handleClose: PropTypes.func,
}

export default withStyles(style)(ModalStyled)
