const style = theme => ({
  root: {
    padding: '5px 25px',
    textTransform: 'unset',
    fontSize: 15,
  },
  rounded: {
    borderRadius: 30,
  },
})

export default style