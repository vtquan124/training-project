import React from 'react'
import PropTypes from 'prop-types'
import { withStyles, Button } from '@material-ui/core'
import style from './style'
import clsx from 'clsx'

const ButtonStyled = ({label, classes, rounded, color, handleClick, variant, ...props }) => {
  const hasRounded = rounded ? `${classes.rounded}` : ''
  return (
    <Button
      variant={variant}
      className={clsx(classes.root, hasRounded)}
      color={color}
      onClick={handleClick}
      {...props}
    >
      {label}
    </Button>
  )
}

ButtonStyled.defaultProps = {
  variant: 'contained',
  label: '',
  color: 'primary',
  rounded: false,
  handleClick:()=>{}
}

ButtonStyled.propTypes = {
  classes: PropTypes.shape().isRequired,
  variant: PropTypes.string,
  label: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.any,
  ]).isRequired,
  color: PropTypes.string,
  rounded: PropTypes.bool,
  handleClick: PropTypes.func
}

export default withStyles(style)(ButtonStyled)
