import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { withStyles, MenuItem,TextField } from '@material-ui/core'
import style from './style'

const SelectInput = ({ classes, label, dataValue, variant, ...props }) => {
  const [value, setValue] = useState('')

  const handleChange = (e) => {
    setValue(e.target.value)
  }

  return (
    <TextField
      className={classes.root}
      select
      label={label}
      variant={variant}
      value={value}
      onChange={handleChange}
      {...props}
    >
      {dataValue.map(option => (
        <MenuItem key={option.value} value={option.value}>
          {option.label}
        </MenuItem>
      ))}
    </TextField>
  )
}

SelectInput.defaultProps = {
  classes: '',
  label: '',
  variant:'outlined',
  dataValue: [],
}

SelectInput.propTypes = {
  classes: PropTypes.shape().isRequired,
  label: PropTypes.string.isRequired,
  dataValue: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string,
      value: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.bool,
      ])
    })
  ),
}

export default withStyles(style)(SelectInput)
