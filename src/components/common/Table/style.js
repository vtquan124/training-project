const style = theme => ({
  table: {
    width: '100%'
  },
  tableHeader: {
    backgroundColor: theme.palette.action.selected,
    color: theme.palette.text.primary,
  },
  desc: {
    width: 250,
  },
})

export default style