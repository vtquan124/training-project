import React from 'react'
import PropTypes from 'prop-types'
import {
  withStyles,
  Table,
  TableBody,
  TableCell,
  TableContainer, 
  TableHead,
  TableRow,
  IconButton,
  Typography,
} from '@material-ui/core'
import DeleteIcon from '@material-ui/icons/Delete'
import style from './style'

const createData = (id, name, qty, type, reason, desc) => {
  if (type === 'Increase') {
    qty = `(+)${qty}`
  } else {
    qty = `(-)${qty}`
  }
  return {id, name, qty, type, reason, desc}
}

const dummyData = [
  createData(2, 'Amy', 22, 'Decrease', 'Sell to Customer', "..."),
  createData(3, 'Amy', 2, 'Increase', 'Purchase form Provider', "..."),
  createData(4, 'Amy', 12, 'Decrease', 'Purchase form Provider', "This is a form for displaying"),
]

const TableDetail = ({ classes, data, handleClick, ...props }) => {
  return (
    <TableContainer>
      <Table className={classes.table}>
        <TableHead className={classes.tableHeader}>
          <TableRow>
            <TableCell align='left'>Operator</TableCell>
            <TableCell align='left'>Qty</TableCell>
            <TableCell align='left'>Adjust Type</TableCell>
            <TableCell align='left'>Adjust Reason</TableCell>
            <TableCell align='left'>Description</TableCell>
            <TableCell align='left'>Action</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {
            dummyData.map(row => (
            <TableRow key={row.id}>
              <TableCell align='left'>{row.name}</TableCell>
              <TableCell align='left'>{row.qty}</TableCell>
              <TableCell align='left'>{row.type}</TableCell>
              <TableCell align='left'>{row.reason}</TableCell>
              <TableCell align='left'>
                <Typography noWrap className={classes.desc}>{row.desc}</Typography>
              </TableCell>
              <TableCell align='left'>
                <IconButton color='secondary' onClick={handleClick}>
                  <DeleteIcon/>
                  </IconButton>
              </TableCell>
          </TableRow>
            ))
          }
          
        </TableBody>
      </Table>
    </TableContainer>
  )
}

TableDetail.defaultProps = {
  data: [],
  handleClick: ()=>{},
}

TableDetail.propTypes = {
  classes: PropTypes.shape().isRequired,
  data: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number,
      name: PropTypes.string,
      qty: PropTypes.number,
      reason: PropTypes.string,
      desc: PropTypes.string
    })),
    handleClick: PropTypes.func,
}

export default withStyles(style)(TableDetail)
