import React from 'react'
import PropTypes from 'prop-types'
import { withStyles,TextField } from '@material-ui/core'
import style from './style'

const TextInput = ({ classes, type, variant, label, ...props }) => {
  return (
    <TextField
      className={classes.root}
      variant={variant}
      type={type}
      label={label}
      {...props}
    />
  )
}

TextInput.defaultProps = {
  variant: 'outlined',
  type: 'text',
  label: '',
}

TextInput.propTypes = {
  classes: PropTypes.shape().isRequired,
  variant: PropTypes.string,
  type: PropTypes.string,
  label: PropTypes.string.isRequired,
  handleChange: PropTypes.func
}

export default withStyles(style)(TextInput)
