import React from 'react'
import PropTypes from 'prop-types'
import SelectInput from '../common/Select/index.js'
import {getIn} from 'formik'

const Input = ({ field, form, inputProps, ...props }) => {
  return (
    <SelectInput
      error={!!getIn(form.touched, field.name) && !!getIn(form.errors, field.name)}
      helperText={getIn(form.touched, field.name) && getIn(form.errors, field.name)}
      inputProps={{
        ...field,
        ...inputProps,
        value: field.value || ''
      }}
      {...props}
    />
  )
}

Input.defaultProps = {
  inputProps: {},
}

Input.propTypes = {
  field: PropTypes.shape().isRequired,
  form: PropTypes.shape().isRequired,
  inputProps: PropTypes.shape()
}

export default Input
