import React from 'react'
import PropTypes from 'prop-types'
import TextInput from '../common/TextInput/index'
import {getIn} from 'formik'

const Input = ({ field, form, inputProps, ...props }) => {
  return (
    <TextInput
      error={!!getIn(form.touched, field.name) && !!getIn(form.errors, field.name)}
      helperText={getIn(form.touched, field.name) && getIn(form.errors, field.name)}
      // Only using with <Field children/>
      // error={meta.touched && !!meta.error}
      // helperText={meta.touched && meta.error}
      inputProps={{
        ...field,
        ...inputProps,
        value: field.value || ''
      }}
      {...props}
    />
  )
}

Input.defaultProps = {
  inputProps: {},
}

Input.propTypes = {
  field: PropTypes.shape().isRequired,
  form: PropTypes.shape().isRequired,
  inputProps: PropTypes.shape()
}

export default Input
