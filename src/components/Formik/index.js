//export all Formik elements
import FieldWrapper from '../../utils/formik/FieldWrapper'

import Input from '../Formik/Input'
import Select from '../Formik/Select'

export const Field = {
  Input: FieldWrapper(Input),
  Select: FieldWrapper(Select),
}

