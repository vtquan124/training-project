import React from 'react'
import PropTypes from 'prop-types'
import { withStyles, Grid } from '@material-ui/core'
import style from './style'
import {Field} from '../../Formik/index'
import { Formik, Form } from 'formik'
import * as Yup from 'yup'
import { schemas } from '../../../utils/schemas/index'
import Button from '../../common/Button/index'

const AdjustmentForm = ({ classes,...props }) => {

  // Data form
  const products = [
    {
      label: 'Shoe',
      value: 'shoe',
    },
    {
      label: 'T-shirt',
      value: 't_shirt',
    },
    {
      label: 'Hat',
      value: 'hat',
    }
  ]
  const adjustType = [
    {
      label: 'Increase',
      value: 'increase',
    },
    {
      label: 'Decease',
      value: 'decrease',
    }
  ]
  const adjustReason = [
    {
      label: 'Purchase from provider',
      value: 'purchase_from_provider',
    },
    {
      label: 'Sell to customer',
      value: 'sell_to_customer',
    }
  ]
  const initialValues = {
    adjustReason: '',
    adjustType: '',
    product: '',
    quantity: '',
  }

  return (
    <Grid container>
      <Grid item container xs={12} lg={8}>
        <Formik
          initialValues={ initialValues }
          validationSchema={Yup.object().shape(schemas)}
          enableReinitialize
          onSubmit={(values, { resetForm }) => {
            console.log(values);
            resetForm()
        }}
        >
          <Form className={classes.form}>
            <Grid className={classes.gridContainer} item container xs={12} spacing={5}>
              <Grid className={classes.gridItem} item container xs={12} md={6} lg={12} spacing={5}>
                <Grid className={classes.gridItemXs} item xs={12} lg={6}>
                  <Field.Select
                    name='product'
                    label="Product"
                    dataValue={products}
                  />
                </Grid>
                <Grid className={classes.gridItemXs} item xs={12} lg={6}>
                  <Field.Input
                    name='quantity'
                    label='Qty'
                    type='number'
                  />
                </Grid>
              </Grid>
              <Grid className={classes.gridItem} item container xs={12} md={6} lg={12} spacing={5}>
                <Grid className={classes.gridItemXs} item xs={12} lg={6}>
                  <Field.Select
                  name='adjustType'
                  label="Adjust Type"
                  dataValue={adjustType}
                />
                </Grid>
                <Grid className={classes.gridItemXs} item xs={12} lg={6}>
                  <Field.Select
                  name='adjustReason'
                  label="Adjust Reason"
                  dataValue={adjustReason}
                />
                </Grid>
              </Grid>
              <Grid className={classes.gridItem} item container xs={12} spacing={5}>
                <Grid className={classes.gridItemXs} item xs={12} lg={6}>
                  <Field.Input
                  name='description'
                  label='Description'
                  multiline
                  rows={4}
                  />
                  <Button className={classes.addBtn} type='submit' label='+Add' color='default' />
                </Grid>
              </Grid>
            </Grid>
          </Form>
        </Formik>
      </Grid>
    </Grid>
  )
}

AdjustmentForm.defaultProps = {
  classes: '',
}

AdjustmentForm.propTypes = {
  classes: PropTypes.shape().isRequired,
}

export default withStyles(style)(AdjustmentForm)
