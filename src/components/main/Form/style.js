const style = theme => ({
  form: {
    width: '100%',
  },
  addBtn: {
    marginTop: theme.spacing(4)
  },
  gridContainer: {
    [theme.breakpoints.down('md')]: {
      padding: theme.spacing(0),
      margin: theme.spacing(0)
    }
  },
  gridItem: {
    [theme.breakpoints.down('md')]: {
      padding: '0 !important',
      margin: theme.spacing(0),
    }
  },
  gridItemXs: {
    [theme.breakpoints.down('sm')]: {
      padding: '20px 0 !important',
    }
  },
})

export default style
