import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { withStyles, Grid, Typography } from '@material-ui/core'
import style from './style'
import TableDetail from '../../common/Table/index'
import ModalStyled from '../../common/Modal/index'

const AdjustmentDetail = ({ classes, ...props }) => {
  const [open, setOpen] = useState(false)

  const handleOpen = () => {
    setOpen(true)
  }

  const handleClose = () => {
    setOpen(false)
  }

  // Render whole the tables
  return (
    <div>
      <Grid container spacing={7}>
        <Grid item xs={12}>
          <Typography variant='h6' color='primary'>Adjustment Details</Typography>
        </Grid>
        <Grid item container xs={12}>
          <Grid item xs={12}>
            <Typography className={classes.productTitle} variant='body1' gutterBottom>Shoe</Typography>
          </Grid>
          <Grid item xs={12}>
            <TableDetail handleClick={handleOpen}/>
          </Grid>
        </Grid>
        <Grid item container xs={12}>
          <Grid item xs={12}>
            <Typography className={classes.productTitle} variant='body1' gutterBottom>T-shirt</Typography>
          </Grid>
          <Grid item xs={12}>
            <TableDetail handleClick={handleOpen}/>
          </Grid>
        </Grid>
        <Grid item container xs={12}>
          <Grid item xs={12}>
            <Typography className={classes.productTitle} variant='body1'gutterBottom>Hat</Typography>
          </Grid>
          <Grid item xs={12}>
            <TableDetail handleClick={handleOpen}/>
          </Grid>
        </Grid>
      </Grid>
      <ModalStyled open={open} handleClose={handleClose}/>
      </div>
  )
}

AdjustmentDetail.defaultProps = {
  classes:'',
}

AdjustmentDetail.propTypes = {
  classes: PropTypes.shape().isRequired,
}

export default withStyles(style)(AdjustmentDetail)

